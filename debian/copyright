Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: taskcoach
Upstream-Contact: Task Coach developers <developers@taskcoach.org>
Files-Excluded:
 *.pyc
 .hgignore
 CHANGES.txt
 INSTALL.txt
 LICENSE.txt
 MANIFEST
 README.txt
 build
 build.in/portableapps
 build.in/winpenpack
 dist
 dist.in
 extension
 i18n.in/messages.pot
 taskcoachlib/bin.in
 taskcoachlib/gui/icons.py
 taskcoachlib/i18n/??.py
 taskcoachlib/i18n/???.py
 taskcoachlib/i18n/??_??.py
 taskcoachlib/persistence/xml/templates.py
 taskcoachlib/thirdparty/aui
 taskcoachlib/thirdparty/chardet
 taskcoachlib/thirdparty/combotreebox.py
 taskcoachlib/thirdparty/customtreectrl.py
 taskcoachlib/thirdparty/dateutil
 taskcoachlib/thirdparty/desktop
 taskcoachlib/thirdparty/gntp
 taskcoachlib/thirdparty/hypertreelist.py
 taskcoachlib/thirdparty/keyring
 taskcoachlib/thirdparty/lockfile
 taskcoachlib/thirdparty/ntlm
 taskcoachlib/thirdparty/pubsub
 taskcoachlib/thirdparty/pybonjour.py
 taskcoachlib/thirdparty/pyparsing.py
 taskcoachlib/thirdparty/squaremap
 taskcoachlib/thirdparty/wmi.py
 taskcoachlib/thirdparty/wxScheduler
 taskcoachlib/thirdparty/xdg
 tests/disttests/win32/sendinput/sendinput.pyd
 thirdparty/PyPubSub-*
 thirdparty/SquareMap-*.tar.gz
 thirdparty/agw-*
 thirdparty/aui-*
 thirdparty/chardet-*.tar.gz
 thirdparty/keyring-*.tar.gz
 thirdparty/lockfile-*
 thirdparty/pyparsing-*.tar.gz
 thirdparty/python-dateutil-*.tar.gz
 thirdparty/pyxdg-*.tar.gz
 tools/pygettext.py
 tools/webchecker.py
 website.in
Source: https://www.taskcoach.org
 Upstream provides many tarballs. We use
 the "raw source tar archive". Please see debian/watch for details.
 .
 These files are removed from the upstream archive:
 .
 * build.in/portableapps:
 Contains a GPL-2 copy, but some files mention the BSD license.
 Windows binary: TaskCoachPortable.exe.
 See http://portableapps.com/.
 .
 * build.in/winpenpack:
 Non DFSG license (winPenPack License Agreement v1.0)
 Windows binary: X-TaskCoach.exe.
 See http://www.winpenpack.com/en/news.php.
 .
 * dist.in:
 Windows binaries: *.dll (Visual 9 runtime).
 .
 * extension/macos:
 Mac binaries: *.so (convenience for developers without xcode).
 .
 * i18n.in/messages.pot:
 Generated during compilation, mentioning the build date.
 Upstream insists on keeping it for Launchpad integration.
 .
 * taskcoachlib/bin.in:
 Mac and Windows binaries: *.so *.pyd (python dynamic library).
 Upstream intends to drop pysyncml dependency for license reasons anyways.
 .
 * tests/disttests/win32/sendinput/sendinput.pyd:
 Windows binary (python dynamic library).
 .
 * tools/webchecker.py:
 No license, but a copyright claimer.
 Obsolete, once used by upstream to check website availability.
 .
 * website.in:
 Part only used to rebuild the upstream website.
 website.in/css/jquery.lightbox-0.5.css and
 website.in/js/jquery.lightbox-0.5.min.css have a suspicious license
 (CCAttribution-ShareAlike 2.5 Brazil
 http://creativecommons.org/licenses/by-sa/2.5/br/deed.en_US).
 .
 Some thirdparty dependencies are packaged in Debian:
 aui (from python-wxgtk)
 chardet
 combotreebox (1.1 from python-wxgtk, instead of embedded 1.2)
 customtreectrl.py and hypertreelist.py (from agw in python-wxgtk)
 dateutil
 keyring
 lockfile
 pubsub (from python-wxgtk)
 pygettext.py (from python)
 pyparsing
 squaremap
 xdg.
 The embedded source is removed to gain space, ensure that is will
 never be used unintentionally and avoid noise in the VCS.
 For each one,
 copyright/Files-Excluded removes a tarball,
 control/Build-Depends and Depends require a minimal version,
 a patch does the rest.

Files: *
Copyright: 2004-2019 Task Coach developers <developers@taskcoach.org>
           2014 Joseph Wang https://sourceforge.net/u/drjoe/profile
           2014 Tom Monaco http://www.thomasmonaco.com
           2013      Ivan Romanov <https://sourceforge.net/u/ivanromanov/>
           2012-2013 Aaron Wolf <wolftune@gmail.com>
           2005-2013 Jérôme Laheurte <fraca7@free.fr>
           2004-2013 Frank Niessink <frank@niessink.com>
           2012      Nicola Chiapolini <nicola.chiapolini@physik.uzh.ch>
           2011      David Harks <dave@dwink.net>
           2010      Svetoslav Trochev <sal_electronics@hotmail.com>
           2009      George Weeks <gcw52@telus.net>
           2008      Rob McMullen <rob.mcmullen@gmail.com>
           2008      Carl Zmola <zmola@acm.org>
           2008      Thomas Sonne Olesen <tpo@sonnet.dk>
           2008      João Alexandre de Toledo <jtoledo@griffo.com.br>
           2008      Marcin Zajaczkowski <mszpak@wp.pl>
           2011      Tobias Gradl <https://sourceforge.net/users/greentomato>
           2005-2013 Rosetta Contributors and Canonical Ltd
           2014      Manab Chetia <manab.chetia@outlook.com>
           2015      Nicola https://sourceforge.net/u/swisscarbon/profile
           2016      Roger (rogerdc@gmail.com)
License: GPL-3+

Files: build.in/debian/taskcoach.appdata.xml
Copyright: 2014-2018 Task Coach developers <developers@taskcoach.org>
License: CC0-1.0
 See /usr/share/common-licenses/CC0-1.0.

Files: debian/*
Copyright: 2009      Alejandro Garrido Mota <garridomota@gmail.com>
           2012-2019 Nicolas Boulenguez <nicolas@debian.org>
License: GPL-3+

Files: icons.in/nuvola.zip
Copyright: 2003-2013 David Vignoni (david@icon-king.com)
License: LGPL-2.1
Comment:
 No explicit version, last update 1.3.29 -> 1.3.31.

Files: taskcoachlib/thirdparty/_weakrefset.py
Copyright: 2013-2013 Task Coach developers <developers@taskcoach.org>
License: GPL-3+
Comment:
 This code is separated-out because it is needed
 by abc.py to load everything else at startup.

Files: taskcoachlib/thirdparty/deltaTime.py
Copyright: 2000-2012 Paul McGuire
License: MIT
Comment:
 The license is not explicit, but the file is very similar to an
 example provided with python-pyparsing by the same author, so we can
 assume the same license.

Files: taskcoachlib/thirdparty/guid.py
Copyright: 2006-2012 Conan C. Albrecht
License: MIT
Comment: See taskcoachlib/thirdparty/README.txt.

Files: taskcoachlib/thirdparty/smartdatetimectrl.py
Copyright: 2012-2018 Jerome Laheurte <fraca7@free.fr>
           2012-2012 Frank Niessink <frank@niessink.com>
License: GPL-3+
Comment: See taskcoachlib/thirdparty/README.txt.

Files: taskcoachlib/thirdparty/snarl.py
Copyright: 2012-2012 Alexander Lash <alexander.lash@gmail.com>
License: GPL-2
Comment:
 Quoting the author in a mail exchange (end 2011): It would be my
 pleasure to re-release this code under an explicit license. Would the
 GPLv2 be acceptable? If so, please consider this e-mail a licensing
 of the code under the GPLv2. I'll publish an updated version of
 snarl.py with license included on my github. Very excited about
 inclusion. :)

Files: taskcoachlib/thirdparty/timeline/*
Copyright: 2009-2009 Frank Niessink <frank@niessink.com>
                     Mike C. Fletcher <mcfletch@vrplumber.com>
License: MIT
Comment: See taskcoachlib/thirdparty/timeline/license.txt.

Files: thirdparty/wxScheduler-*
Copyright: 2009-2013 Esposti Daniele: expo --at-- expobrain -dot- net
           2009-2013 Michele Petrazzo: michele -dot- petrazzo --at-- unipex -dot- it
           2009-2014 Jérôme Laheurte: fraca7 --at-- free -dot- fr
License: wxwindows
Comment: See readme.txt in thirdparty/wxScheduler-r150.tar.gz,
 and http://www.wxwidgets.org/about/newlicen.htm.

Files: thirdparty/desktop-*
Copyright: 2005-2013 Paul Boddie <paul@boddie.org.uk>
           2012-2013 Jérôme Laheurte <fraca7@free.fr>
License: LGPL-3+
Comment:
 See thirdparty/README.txt.
 Note that this module requires xprop from the x11-utils package.

Files: thirdparty/gntp-*
Copyright: 2011-2011 Paul Traylor
License: MIT

Files: thirdparty/pybonjour-*
Copyright: 2007-2008 Christopher J. Stawarz
License: MIT

Files: thirdparty/python-ntlm-*
Copyright: 2001-2011 Ben Dyer <ben.dyer@taguchimail.com>
           2001-2011 Dmitry A. Rozmanov <dima@xenon.spb.ru>
           2001-2011 Matthijs Mullender <info@zopyx.org>
License: LGPL-3+
Comment:
 Debian packages 1.0.1, missing IMAP support.
 .
 See thirdparty/README.txt.

Files: thirdparty/WMI-*
Copyright: 2003-2003 Tim Golden <mail@timgolden.me.uk>
License: MIT

License: GPL-2
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation version 2.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation, version 2.1 of the
 License.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License: LGPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see
 <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 3 can be found in
 "/usr/share/common-licenses/LGPL-3".

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
 PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
 FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: wxwindows
                wxWindows Library Licence, Version 3.1
                ======================================
  .
  Copyright (C) 1998-2005 Julian Smart, Robert Roebling et al
  .
  Everyone is permitted to copy and distribute verbatim copies
  of this licence document, but changing it is not allowed.
  .
                       WXWINDOWS LIBRARY LICENCE
     TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
  .
  This library is free software; you can redistribute it and/or modify it
  under the terms of the GNU Library General Public Licence as published by
  the Free Software Foundation; either version 2 of the Licence, or (at
  your option) any later version.
  .
  This library is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
  General Public Licence for more details.
  .
  You should have received a copy of the GNU Library General Public Licence
  along with this software, usually in a file named COPYING.LIB.  If not,
  write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
  Boston, MA 02110-1301, USA.
  .
  EXCEPTION NOTICE
  .
  1. As a special exception, the copyright holders of this library give
  permission for additional uses of the text contained in this release of
  the library as licenced under the wxWindows Library Licence, applying
  either version 3.1 of the Licence, or (at your option) any later version of
  the Licence as published by the copyright holders of version
  3.1 of the Licence document.
  .
  2. The exception is that you may use, copy, link, modify and distribute
  under your own terms, binary object code versions of works based
  on the Library.
  .
  3. If you copy code from files distributed under the terms of the GNU
  General Public Licence or the GNU Library General Public Licence into a
  copy of this library, as this licence permits, the exception does not
  apply to the code that you add in this way.  To avoid misleading anyone as
  to the status of such modified files, you must delete this exception
  notice from such code and/or adjust the licensing conditions notice
  accordingly.
  .
  4. If you write modifications of your own for this library, it is your
  choice whether to permit this exception to apply to your modifications.
  If you do not wish that, you must delete the exception notice from such
  code and/or adjust the licensing conditions notice accordingly.
